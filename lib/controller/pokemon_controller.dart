import 'dart:convert' as convert;
import 'package:pokedexserver/model/PokemonDetailsResponse.dart';
import 'package:pokedexserver/pokedexserver.dart';
import 'package:http/http.dart' as http;

class PokemonController extends ResourceController {
  @Operation.get()
  Future<Response> getPokemons(
      @Bind.query('limit') int limit, @Bind.query('offset') int offset) async {
    try {
      List<PokemonDetailsResponse> pokemonsWithDetails = [];
      final url =
          "https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}";
      print(url);
      final response = await http.get(url);

      final jsonResponse =
          convert.jsonDecode(response.body) as Map<String, dynamic>;
      final results = jsonResponse['results'] as List;
      for (int i = 0; i < results.length; i++) {
        final result = results[i];
        final pokemonDetailResponse =
            await http.get(result['url']);
        final decoded = convert.jsonDecode(pokemonDetailResponse.body) as Map<String, dynamic>;
        pokemonsWithDetails.add(PokemonDetailsResponse.fromJson(decoded));
      }

      final List<Map<String, dynamic>> mapped = [];
      for (int i = 0; i < pokemonsWithDetails.length; i++) {
        final details = pokemonsWithDetails[i];
        mapped.add(details.toJson());
      }

      print(mapped.length);
      return Response.ok({"count": jsonResponse["count"], "data": mapped});
    } catch (e) {
      print(e);
      throw Response.badRequest();
    }
  }
}
