class ListResult {
  final String name;
  final String url;

  ListResult({this.name, this.url});

  factory ListResult.fromJson(Map<String, dynamic> json) {
    return ListResult(
      name: json['name'] as String,
      url: json['url'] as String
    );
  }

}
