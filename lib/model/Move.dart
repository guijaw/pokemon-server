import 'MoveX.dart';

class Move {
  MoveX move;

  Move(this.move);

  factory Move.fromJson(Map<String, dynamic> json) {
    return Move(
      json['move'] != null
          ? MoveX.fromJson(json['move'] as Map<String, dynamic>)
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "move": move.toJson(),
    };
  }
}
