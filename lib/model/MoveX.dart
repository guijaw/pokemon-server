class MoveX {
  String name;
  String url;

  MoveX({this.name, this.url});

  factory MoveX.fromJson(Map<String, dynamic> json) {
    return MoveX(
      name: json['name'] as String,
      url: json['url'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "url": url,
    };
  }
}
