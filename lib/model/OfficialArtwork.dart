class OfficialArtwork {
  final String front_default;

  OfficialArtwork({this.front_default});

  factory OfficialArtwork.fromJson(Map<String, dynamic> json) {
    return OfficialArtwork(
      front_default: json['front_default'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {"front_default": front_default};
  }
}
