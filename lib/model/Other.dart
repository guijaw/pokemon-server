import 'OfficialArtwork.dart';

class Other {
  OfficialArtwork officialArtwork;

  Other(this.officialArtwork);

  factory Other.fromJson(Map<String, dynamic> json) {
    return Other(
      json['official-artwork'] != null
          ? OfficialArtwork.fromJson(
              json['official-artwork'] as Map<String, dynamic>)
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "officialArtwork": officialArtwork.toJson(),
    };
  }
}
