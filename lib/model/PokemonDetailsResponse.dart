import 'Move.dart';
import 'Sprites.dart';
import 'Type.dart';

class PokemonDetailsResponse {
  final int id;
  final List<Move> moves;
  final String name;
  final Sprites sprites;
  final List<Type> types;

  PokemonDetailsResponse(
      {this.id, this.moves, this.name, this.sprites, this.types});

  factory PokemonDetailsResponse.fromJson(Map<String, dynamic> json) {
    return PokemonDetailsResponse(
      id: json['id'] as int,
      moves: json['moves'] != null
          ? (json['moves'] as List)
              .map((i) => Move.fromJson(i as Map<String, dynamic>))
              .toList()
          : null,
      name: json['name'] as String,
      sprites: json['sprites'] != null
          ? Sprites.fromJson(json['sprites'] as Map<String, dynamic>)
          : null,
      types: json['types'] != null
          ? (json['types'] as List)
              .map((i) => Type.fromJson(i as Map<String, dynamic>))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final List<Map<String,dynamic>> movesMap = [];
    final List<Map<String,dynamic>> typesMap = [];
    types.map((type) => typesMap.add(type.toJson()));
    moves.map((move) => movesMap.add(move.toJson()));
    return {
      "id": id,
      "name": name,
      "moves": movesMap,
      "sprites": sprites.toJson(),
      "types": typesMap,
    };
  }
}
