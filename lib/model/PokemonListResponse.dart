import 'package:pokedexserver/model/ListResult.dart';

class PokemonListResponse {
    int count;
    String next;
    String previous;
    List<ListResult> results;

    PokemonListResponse({this.count, this.next, this.previous, this.results});

    factory PokemonListResponse.fromJson(Map<String, dynamic> json) {
        return PokemonListResponse(
            count: json['count'] as int,
            next: json['next'] as String,
            previous: json['previous'] as String,
            results: json['results'] != null ? (json['results'] as List).map((i) => ListResult.fromJson(i as Map<String, dynamic>)).toList() : null,
        );
    }
}
