import 'Other.dart';

class Sprites {
  Other other;

  Sprites(this.other);

  factory Sprites.fromJson(Map<String, dynamic> json) {
    return Sprites(
      json['other'] != null
          ? Other.fromJson(json['other'] as Map<String, dynamic>)
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "other": other.toJson(),
    };
  }
}
