import 'TypeX.dart';

class Type {
  int slot;
  TypeX type;

  Type({this.slot, this.type});

  factory Type.fromJson(Map<String, dynamic> json) {
    return Type(
      slot: json['slot'] as int,
      type: json['type'] != null
          ? TypeX.fromJson(json['type'] as Map<String, dynamic>)
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "slot": slot,
      "type": type.toJson(),
    };
  }
}
