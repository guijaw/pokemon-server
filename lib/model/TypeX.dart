class TypeX {
  String name;
  String url;

  TypeX({this.name, this.url});

  factory TypeX.fromJson(Map<String, dynamic> json) {
    return TypeX(
      name: json['name'] as String,
      url: json['url'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "url": url,
    };
  }
}
